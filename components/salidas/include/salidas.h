#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#define SAL_1 5
#define SAL_2 12
#define SAL_3 19
#define SAL_4 23
#define SAL_5 2
#define SAL_6 15
#define SAL_7 0

#define ENT_1 3 // Tiene una resistencia de pullup interna
#define ENT_2 36
#define ENT_3 18
#define ENT_4 4
#define ENT_5 27
#define ENT_6 14
#define ENT_7 13

#define FIL_1 26  
#define FIL_2 25 
#define FIL_3 33
#define FIL_4 32
#define COL_1 35 
#define COL_2 34
#define COL_3 39
// #define COL_4 36



