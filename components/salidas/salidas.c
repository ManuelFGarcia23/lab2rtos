#include "salidas.h"



void salidas(void *pvParameter){
    while(1){

        vTaskDelay(100 / portTICK_PERIOD_MS);
        if(!gpio_get_level(ENT_1)){
            gpio_set_level(SAL_1, 1);
        }else
        {
            gpio_set_level(SAL_1, 0);
        }

        if(gpio_get_level(ENT_2)){
            gpio_set_level(SAL_2, 1);
        }else
        {
            gpio_set_level(SAL_2, 0);
        }

        if(gpio_get_level(ENT_3)){
            gpio_set_level(SAL_3, 1);
        }else
        {
            gpio_set_level(SAL_3, 0);
        }

        if(gpio_get_level(ENT_4)){
            gpio_set_level(SAL_4, 1);
        }else
        {
            gpio_set_level(SAL_4, 0);
        }

        if(gpio_get_level(ENT_5)){
            gpio_set_level(SAL_5, 1);
        }else
        {
            gpio_set_level(SAL_5, 0);
        }

        if(gpio_get_level(ENT_6)){
            gpio_set_level(SAL_6, 1);
        }else
        {
            gpio_set_level(SAL_6, 0);
        }

        if(gpio_get_level(ENT_7)){
            gpio_set_level(SAL_7, 1);
        }else
        {
            gpio_set_level(SAL_7, 0);
        }
        

   
    }
}


