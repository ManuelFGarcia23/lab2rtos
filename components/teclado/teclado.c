#include "teclado.h"


char obtener_tecla(void);
char tecla;
char teclas[4][3] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'*', '0', '#'}
};
int filas[4] = {FIL_1, FIL_2, FIL_3, FIL_4};
int columnas[3] = {COL_1, COL_2, COL_3};
int teclas_mem[4][3] = {
    {0, 0, 0},
    {0, 0, 0},
    {0, 0, 0}
};
int val;
char obtener_tecla(void){
    for (int i = 0; i < 4; i++)
    {
        vTaskDelay(20 / portTICK_PERIOD_MS);
        gpio_set_level(filas[i], 1); 
        for (int j = 0; j < 3; j++)
        {    
            val = gpio_get_level(columnas[j]);
            if(!teclas_mem[i][j]&& val){
                
                gpio_set_level(filas[i], 0);
                teclas_mem[i][j] = val;
                return(teclas[i][j]);        
            }
            teclas_mem[i][j] = val;
        };
        gpio_set_level(filas[i], 0);
        
    };
    return('n');
}


void teclado(void *pvParameter){
    while(1) 
    {

        tecla = obtener_tecla();
        if (tecla == 'n'){
            // gpio_set_level(SAL_8, 0);
            continue;
        }else
        {
            // gpio_set_level(SAL_8, 1);
            // continue;
            printf("oooooooo La tecla oprimida fue: %c\n", tecla);
        }
       
    }

}