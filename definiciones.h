#define SAL_1 12
#define SAL_2 1 // Tiene un parpadeo constante
#define SAL_3 5
#define SAL_4 17
#define SAL_5 16
#define SAL_6 0
#define SAL_7 2
#define SAL_8 15

#define ENT_1 23
#define ENT_2 3 // Tiene una resistencia de pullup interna
#define ENT_3 19
#define ENT_4 18
#define ENT_5 4
#define ENT_6 27
#define ENT_7 14
#define ENT_8 13

//Tecaldo 4x4
#define FIL_1 36
#define FIL_2 39
#define FIL_3 34
#define FIL_4 35
#define COL_1 32
#define COL_2 33
#define COL_3 25
#define COL_4 26