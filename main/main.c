#include "main.h"

void app_main()
{
    
    gpio_pad_select_gpio(SAL_1);
    gpio_pad_select_gpio(SAL_2);
    gpio_pad_select_gpio(SAL_3);
    gpio_pad_select_gpio(SAL_4);
    gpio_pad_select_gpio(SAL_5);
    gpio_pad_select_gpio(SAL_6);
    gpio_pad_select_gpio(SAL_7);

    gpio_pad_select_gpio(ENT_1);
    gpio_pad_select_gpio(ENT_2);
    gpio_pad_select_gpio(ENT_3);
    gpio_pad_select_gpio(ENT_4);
    gpio_pad_select_gpio(ENT_5);
    gpio_pad_select_gpio(ENT_6);
    gpio_pad_select_gpio(ENT_7);

    gpio_pad_select_gpio(FIL_1);
    gpio_pad_select_gpio(FIL_2);
    gpio_pad_select_gpio(FIL_3);
    gpio_pad_select_gpio(FIL_4);
    gpio_pad_select_gpio(COL_1);
    gpio_pad_select_gpio(COL_2);
    gpio_pad_select_gpio(COL_3);
    // gpio_pad_select_gpio(COL_4);
    
    gpio_set_direction(ENT_1, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_2, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_3, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_4, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_5, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_6, GPIO_MODE_INPUT);
    gpio_set_direction(ENT_7, GPIO_MODE_INPUT);

    gpio_set_direction(FIL_1, GPIO_MODE_OUTPUT);
    gpio_set_direction(FIL_2, GPIO_MODE_OUTPUT);
    gpio_set_direction(FIL_3, GPIO_MODE_OUTPUT);
    gpio_set_direction(FIL_4, GPIO_MODE_OUTPUT);
    gpio_set_direction(COL_1, GPIO_MODE_INPUT);
    gpio_set_direction(COL_2, GPIO_MODE_INPUT);
    gpio_set_direction(COL_3, GPIO_MODE_INPUT);
    // gpio_set_direction(COL_4, GPIO_MODE_INPUT);


    gpio_set_direction(SAL_1, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_2, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_3, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_4, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_5, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_6, GPIO_MODE_OUTPUT);
    gpio_set_direction(SAL_7, GPIO_MODE_OUTPUT);
    gpio_set_level(SAL_1, 0);
    gpio_set_level(SAL_2, 0);
    gpio_set_level(SAL_3, 0);
    gpio_set_level(SAL_4, 0);
    gpio_set_level(SAL_5, 0);
    gpio_set_level(SAL_6, 0);
    gpio_set_level(SAL_7, 0);
    
    
    void teclado(void *pvParameter);
    xTaskCreate(&teclado,"teclado",10240,NULL,5,NULL);
    void salidas(void *pvParameter);
    xTaskCreate(&salidas,"salidas",10240,NULL,5,NULL);
    
   
}
